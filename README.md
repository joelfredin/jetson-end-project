# Jetson End Project

This project lets you do geometry using your bare hands using Jetson Nano.


## Requirements

To run the program you need

* [Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)

## Installation
